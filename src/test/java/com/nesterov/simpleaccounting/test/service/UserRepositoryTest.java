package com.nesterov.simpleaccounting.test.service;

import com.nesterov.simpleaccounting.entity.UserEntity;
import com.nesterov.simpleaccounting.service.UserServiceImpl;
import com.nesterov.simpleaccounting.test.config.TestDataBaseConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@WebAppConfiguration
public class UserRepositoryTest  {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private UserServiceImpl service;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
    }

    @Test
    public void testSaveUser() throws Exception {
        UserEntity user = new UserEntity();
        user.setLogin("Test1");
        user.setFirstName("FirstName1");
        user.setLastName("LastName1");
        user.setPassword("testtest");

        service.save(user);

        List<UserEntity> users = service.getAll();
        Assert.assertEquals(1, users.size());
        Assert.assertEquals(user.getFirstName(), users.get(0).getFirstName());
    }
}
