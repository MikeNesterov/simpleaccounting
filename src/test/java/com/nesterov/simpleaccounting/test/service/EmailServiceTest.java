package com.nesterov.simpleaccounting.test.service;

import com.nesterov.simpleaccounting.config.WebConfig;
import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.service.EmailServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class EmailServiceTest {

    @Resource
    private EmailServiceImpl emailService;

    @Test
    public void testSaveUser(){
        UserDto user = new UserDto();
        user.setEmail("pchell369@gmail.com");
        user.setFirstName("Mike");
        user.setLastName("Nesterov");
        user.setPassword("12345");

        boolean b = emailService.sendRegistrationEmail(user);

        Assert.assertEquals(true, b);
    }
}
