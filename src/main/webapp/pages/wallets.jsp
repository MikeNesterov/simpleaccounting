<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Wallets</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"/>
</head>

<body>
<%@ include file = "header.jsp" %>

<div class="form-container">

    <h1>Wallets</h1>
    <table>
        <tr>
            <td>Name</td>
            <td>Currency</td>
            <td>Balance</td>
        </tr>
        <%--@elvariable id="wallets" type="java.util.Set"--%>
        <%--@elvariable id="wallet" type="com.nesterov.simpleaccounting.entity.WalletEntity"--%>
        <c:forEach items="${wallets}" var="wallet">
        <tr>
            <td>
                <c:out value="${wallet.name}"/>
            </td>
            <td>
                <c:out value="${wallet.currency}"/>
            </td>
            <td>
                <c:out value="${wallet.balance()}"/>
            </td>
            <td>
                <a href="<c:url value="/api/transactions?wallet_id=${wallet.id}"/>">
                    <input type="button" value="Transactions" class="btn btn-primary btn-sm"/>
                </a>
            </td>
            <td>
                <a href="<c:url value="/api/editwallet?wallet_id=${wallet.id}"/>">
                    <input type="button" value="Edit" class="btn btn-primary btn-sm"/>
                </a>
            </td>
            <td>
                <input type="button" value="Delete" onclick="function checkIt() {
                    if (confirm('Are you really sure you want to delete your wallet?')) {
                        document.location.href = '/api/deletewallet?wallet_id=${wallet.id}';
                    }
                }
                checkIt()"
                       class="btn btn-primary btn-sm"/>
            </td>
        </tr>
        </c:forEach>
    </table>
    <br/>
    <a href="<c:url value="/api/createwallet"/>">
        <input type="button" value="Create" class="btn btn-primary btn-sm"/>
    </a>
</div>
</body>
</html>