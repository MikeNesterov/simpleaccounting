<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Transactions</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"/>
</head>

<body>
<%@ include file = "header.jsp" %>

<div class="form-container">
    <h1>Transactions</h1>
    <table>
        <tr>
            <td>Transaction</td>
            <td>Date</td>
            <td>Value</td>
        </tr>
        <%--@elvariable id="transactions" type="java.util.Set"--%>
        <%--@elvariable id="transaction" type="com.nesterov.simpleaccounting.entity.TransactionEntity"--%>
        <c:forEach items="${transactions}" var="transaction">
            <tr>
                <td>
                    <c:out value="${transaction.comment}"/>
                </td>
                <td>
                    <c:out value="${transaction.simpleDate()}"/>
                </td>
                <td>
                    <c:out value="${transaction.value}"/>
                </td>
                <td>
                    <a href="<c:url value="/api/edittransaction?wallet_id=${transaction.wallet.id}&transaction_id=${transaction.id}"/>">
                        <input type="button" value="Edit" class="btn btn-primary btn-sm"/>
                    </a>
                </td>
                <td>
                    <input type="button" value="Delete" onclick="function checkIt() {
                            if (confirm('Are you really sure you want to delete transaction?')) {
                            document.location.href = '/api/deletetransaction?wallet_id=${transaction.wallet.id}&transaction_id=${transaction.id}';
                            }
                            }
                            checkIt()"
                           class="btn btn-primary btn-sm"/>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td> Balance: </td>
            <td></td>
            <td>
            <jsp:useBean id="wallet" scope="request" type="com.nesterov.simpleaccounting.entity.WalletEntity"/>
            <c:out value="${wallet.balance()}"/></td>
        </tr>
    </table>
    <br/>
    <a href="<c:url value="/api/addtransaction?wallet_id=${param.get('wallet_id')}"/>">
        <input type="button" value="Add" class="btn btn-primary btn-sm"/>
    </a>
    <br/><br/>

    <a href="<c:url value="/api/wallets"/>">
        <input type="button" value="Wallets" class="btn btn-primary btn-sm"/>
    </a>
</div>
</body>
</html>
