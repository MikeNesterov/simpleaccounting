<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>New Password Form</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
</head>

<body>

<div class="form-container">

    <h1>New password</h1>

    <%--@elvariable id="userDto" type="com.nesterov.simpleaccounting.dto.UserDto"--%>
    <form:form method="POST" modelAttribute="userDto" class="form-horizontal">

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="email"></label>
                <div class="col-md-7" class="form-control input-sm">
                    <form:hidden path="email"/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="password"> New password</label>
                <div class="col-md-7">
                    <form:password path="password" id="password" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="password" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="matchingPassword">Confirm Password</label>
                <div class="col-md-7">
                    <form:password path="matchingPassword" id="matchingPassword" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="matchingPassword" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-actions floatRight">
                <input type="submit" value="Submit" class="btn btn-primary btn-sm">
            </div>
        </div>
    </form:form>
</div>
</body>
</html>

