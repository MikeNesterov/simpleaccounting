<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Edit Transaction</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
</head>

<body>
<%@ include file = "header.jsp" %>
<div class="form-container">

    <h1>Edit Transaction</h1>

    <%--@elvariable id="transactionEntity" type="com.nesterov.simpleaccounting.entity.TransactionEntity"--%>
    <form:form method="POST" modelAttribute="transactionEntity" class="form-horizontal">

        <div class="line">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="comment">Comment</label>
                <div class="col-md-7" class="form-control input-sm">
                    <form:input type="text" path="comment" id="comment" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="comment" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="line">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="date">Date</label>
                <div class="col-md-7" class="form-control input-sm">
                    <form:input type="date" path="date" id="date" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="date" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="line">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="value">Value</label>
                <div class="col-md-7" class="form-control input-sm">
                    <form:input type="text" path="value" id="value" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="value" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-actions floatRight">
                <input type="submit" value="Save" class="btn btn-primary btn-sm">
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="form-actions floatRight">
                <a href="<c:url value="/api/transactions?wallet_id=${param.get('wallet_id')}"/>">
                    <input type="button" value="Cancel" class="btn btn-primary btn-sm"/>
                </a>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>