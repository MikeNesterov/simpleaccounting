package com.nesterov.simpleaccounting.controller;

import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.entity.TokenEntity;
import com.nesterov.simpleaccounting.service.EmailService;
import com.nesterov.simpleaccounting.service.TokenService;
import com.nesterov.simpleaccounting.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.validation.Valid;

@Controller
@PropertySource("classpath:messages.properties")
public class RegistrationController {

    @Resource
    private Environment env;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(ModelMap model) {
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@Valid UserDto userDto, BindingResult result, ModelMap model) {

        if (userService.checkUserInDB(userDto.getEmail())) {
            result.addError(new FieldError("userDto", "email", env.getRequiredProperty("failEmail")));
        }

        if(!userDto.getPassword().equals(userDto.getMatchingPassword())){
            result.addError(new FieldError("userDto", "matchingPassword", env.getRequiredProperty("failMatchingPassword")));
        }

        if (result.hasErrors()) {
            return "registration";
        }

        emailService.sendRegistrationEmail(userDto);

        return "redirect:/login?email="+userDto.getEmail();
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration
            (Model model, @RequestParam("token") String token) {

        TokenEntity tokenEntity = tokenService.getToken(token);
        if (tokenEntity == null) {
            model.addAttribute("message", env.getRequiredProperty("token.invalidlink"));
            return "badUser";
        }


        if (!tokenService.validateToken(tokenEntity)) {
            model.addAttribute("message", env.getRequiredProperty("token.timeout"));
            return "badUser";
        }

        userService.confirmRegistration(tokenEntity);

        return "redirect:/login";
    }
}
