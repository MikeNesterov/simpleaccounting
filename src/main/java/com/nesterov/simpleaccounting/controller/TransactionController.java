package com.nesterov.simpleaccounting.controller;

import com.nesterov.simpleaccounting.entity.TransactionEntity;
import com.nesterov.simpleaccounting.entity.WalletEntity;
import com.nesterov.simpleaccounting.service.TransactionService;
import com.nesterov.simpleaccounting.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.TreeSet;

@Controller
public class TransactionController {

    @Autowired
    private WalletService walletService;

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/api/transactions", method = RequestMethod.GET)
    public String transactions(@RequestParam("wallet_id") long wallet_id, ModelMap model) {

        WalletEntity walletEntity = walletService.getById(wallet_id);
        model.addAttribute("transactions", new TreeSet<>(walletEntity.getTransactions()));
        model.addAttribute("wallet", walletEntity);
        return "transactions";
    }

    @RequestMapping(value = "/api/deletetransaction", method = RequestMethod.GET)
    public String deleteTransaction(@RequestParam("wallet_id") long wallet_id,
                               @RequestParam("transaction_id") long transaction_id) {

        transactionService.delete(transaction_id);

        return "redirect:/api/transactions?wallet_id="+wallet_id;
    }

    @RequestMapping(value = "/api/addtransaction", method = RequestMethod.GET)
    public String addTransaction(ModelMap model) {

        model.addAttribute("transactionEntity", new TransactionEntity());

        return "addtransaction";
    }

    @RequestMapping(value = "/api/addtransaction", method = RequestMethod.POST)
    public String addTransaction(@RequestParam("wallet_id") long wallet_id,
                                 @Valid TransactionEntity transactionEntity, BindingResult result) {
        if (result.hasErrors()) {
            return "addtransaction";
        }

        transactionService.addTransaction(wallet_id, transactionEntity);

        return "redirect:/api/transactions?wallet_id="+wallet_id;
    }

    @RequestMapping(value = "/api/edittransaction", method = RequestMethod.GET)
    public String editTransaction(ModelMap model, @RequestParam("transaction_id") long transaction_id) {

        model.addAttribute("transactionEntity", transactionService.getById(transaction_id));

        return "edittransaction";
    }

    @RequestMapping(value = "/api/edittransaction", method = RequestMethod.POST)
    public String editTransaction(@RequestParam("wallet_id") long wallet_id,
                                  @RequestParam("transaction_id") long transaction_id,
                                  @Valid TransactionEntity transactionEntity, BindingResult result) {
        if (result.hasErrors()) {
            return "edittransaction";
        }

        transactionService.updateTransaction(transaction_id, transactionEntity);

        return "redirect:/api/transactions?wallet_id="+wallet_id;
    }
}
