package com.nesterov.simpleaccounting.controller;

import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.entity.TokenEntity;
import com.nesterov.simpleaccounting.service.EmailService;
import com.nesterov.simpleaccounting.service.TokenService;
import com.nesterov.simpleaccounting.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@PropertySource("classpath:messages.properties")
public class AccountingController {

    @Resource
    private Environment env;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private EmailService emailService;

    /**
     * This method handles login GET requests.
     * If users is already logged-in and tries to goto login page again, will be redirected to list page.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        if (isCurrentAuthenticationAnonymous()) {
            return "login";
        } else {
            return "redirect:/api/wallets";
        }
    }

    /**
     * This method handles logout requests.
     * Toggle the handlers if you are RememberMe functionality is useless in your app.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
            //persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }


    /**
     * This method returns true if users is already authenticated [logged-in], else false.
     */
    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getPrincipal().equals("anonymousUser");
    }


    @RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
    public String forgotPassword(ModelMap model) {
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "forgotPassword";
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public String forgotPassword(@Valid UserDto userDto, BindingResult result) {

        if (!userService.checkUserInDB(userDto.getEmail())) {
            result.addError(new FieldError("userDto", "email", env.getRequiredProperty("failLogin")));
        }
        if (result.hasFieldErrors("email")) {
            return "forgotPassword";
        }

        emailService.sendNewPasswordEmail(userDto);

        return "redirect:/login?email="+userDto.getEmail();
    }

    @RequestMapping(value = "/newPassword", method = RequestMethod.GET)
    public String newPassword(ModelMap model, @RequestParam("token") String token) {
        TokenEntity tokenEntity = tokenService.getToken(token);
        if (tokenEntity == null) {
            model.addAttribute("message", env.getRequiredProperty("token.invalidlink"));
            return "badUser";
        }

        if (!tokenService.validateToken(tokenEntity)) {
            model.addAttribute("message", env.getRequiredProperty("token.timeout"));
            return "badUser";
        }

        UserDto userDto = new UserDto(tokenEntity.getUser().getLogin());

        model.addAttribute("userDto", userDto);

        return "newPassword";
    }

    @RequestMapping(value = "newPassword", method = RequestMethod.POST)
    public String newPassword
            (@Valid UserDto userDto, BindingResult result) {

        if(!userDto.getPassword().equals(userDto.getMatchingPassword())){
            result.addError(new FieldError("userDto", "matchingPassword", env.getRequiredProperty("failMatchingPassword")));
        }

        if (result.hasFieldErrors("password")|| result.hasFieldErrors("matchingPassword")) {
            return "newPassword";
        }

        userService.newPassword(userDto);

        return "redirect:/login";
    }

    /**
     * This method handles login GET requests.
     * If users is already logged-in and tries to goto login page again, will be redirected to list page.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        if (isCurrentAuthenticationAnonymous()) {
            return "redirect:/login";
        } else {
            return "redirect:/api/wallets";
        }
    }


}
