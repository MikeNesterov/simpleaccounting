package com.nesterov.simpleaccounting.controller;

import com.nesterov.simpleaccounting.entity.UserEntity;
import com.nesterov.simpleaccounting.entity.WalletEntity;
import com.nesterov.simpleaccounting.security.MyUserPrincipal;
import com.nesterov.simpleaccounting.service.UserService;
import com.nesterov.simpleaccounting.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.TreeSet;

@Controller
@EnableAspectJAutoProxy
public class WalletController {

    @Autowired
    private UserService userService;

    @Autowired
    private WalletService walletService;

    @RequestMapping(value = {"/api/wallets"}, method = RequestMethod.GET)
    public String wallets(ModelMap model, @AuthenticationPrincipal MyUserPrincipal myUserPrincipal) {

        UserEntity user = userService.getById(myUserPrincipal.getUserId());
        model.addAttribute("wallets", new TreeSet<>(user.getWallets()));
        return "wallets";
    }

    @RequestMapping(value = "/api/deletewallet", method = RequestMethod.GET)
    public String deleteWallet(@RequestParam("wallet_id") long wallet_id) {

        walletService.delete(wallet_id);

        return "redirect:/api/wallets";
    }

    @RequestMapping(value = "/api/createwallet", method = RequestMethod.GET)
    public String createWallet(ModelMap model) {

        model.addAttribute("walletEntity", new WalletEntity());

        return "createwallet";
    }

    @RequestMapping(value = "/api/createwallet", method = RequestMethod.POST)
    public String createWallet(@Valid WalletEntity walletEntity, BindingResult result,
                               @AuthenticationPrincipal MyUserPrincipal myUserPrincipal) {
        if (result.hasErrors()) {
            return "createwallet";
        }

        walletService.createWallet(myUserPrincipal.getUserId(), walletEntity);

        return "redirect:/api/wallets";
    }


    @RequestMapping(value = "/api/editwallet", method = RequestMethod.GET)
    public String editWallet(@RequestParam("wallet_id") long wallet_id, ModelMap model) {

        model.addAttribute("walletEntity", walletService.getById(wallet_id));

        return "editwallet";
    }

    @RequestMapping(value = "/api/editwallet", method = RequestMethod.POST)
    public String editWallet(@Valid WalletEntity walletEntity, BindingResult result,
                             @RequestParam("wallet_id") long wallet_id) {
        if (result.hasErrors()) {
            return "editwallet";
        }

        walletService.updateWallet(wallet_id, walletEntity);

        return "redirect:/api/wallets";
    }
}
