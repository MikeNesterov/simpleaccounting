package com.nesterov.simpleaccounting.aspects;

import com.nesterov.simpleaccounting.entity.TransactionEntity;
import com.nesterov.simpleaccounting.entity.UserEntity;
import com.nesterov.simpleaccounting.entity.WalletEntity;
import com.nesterov.simpleaccounting.security.MyUserPrincipal;
import com.nesterov.simpleaccounting.service.UserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AccessAspect {

    @Autowired
    private UserService userService;

    @Before("execution(* com.nesterov.simpleaccounting.controller.*.*(..)) && args(wallet_id ,..)")
    public void accessWallet(JoinPoint joinPoint, long wallet_id) throws Throwable {

        UserEntity user = getCurrentUser();

        for(WalletEntity wallet : user.getWallets()){
            if(wallet.getId() == wallet_id)
                return;
        }
        throw new AccessDeniedException("Permission Denied");
    }

    @Before("execution(* com.nesterov.simpleaccounting.controller.*.*(..)) && args(*,transaction_id ,..)")
    public void accessTransaction(JoinPoint joinPoint, long transaction_id) throws Throwable {

        UserEntity user = getCurrentUser();

        for(WalletEntity wallet : user.getWallets()){
            for(TransactionEntity transaction : wallet.getTransactions()){
                if(transaction_id == transaction.getId())
                    return;
            }
        }
        throw new AccessDeniedException("Permission Denied");
    }

    public UserEntity getCurrentUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity user;
        if (principal instanceof MyUserPrincipal) {
            long user_id = ((MyUserPrincipal)principal).getUserId();
            user = userService.getById(user_id);
        } else {
            throw new AuthenticationServiceException("Permission Denied");
        }

        return user;
    }
}
