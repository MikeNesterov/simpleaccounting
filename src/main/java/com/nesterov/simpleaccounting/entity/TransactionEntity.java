package com.nesterov.simpleaccounting.entity;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity
@Table(name="transaction")
public class TransactionEntity implements Serializable, Comparable<TransactionEntity> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "comment")
    @NotEmpty
    private String comment;

    @Column(name = "value")
    @Digits(integer=10, fraction=2)
    private double value;

    @Column (name = "date")
    @NotEmpty
    private String date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallet_id")
    private WalletEntity wallet;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public WalletEntity getWallet() {
        return wallet;
    }

    public void setWallet(WalletEntity wallet) {
        this.wallet = wallet;
    }

    public String simpleDate(){
        return date.substring(0,10);
    }

    @Override
    public int compareTo(TransactionEntity o) {
        return (int)(this.id-o.id);
    }
}
