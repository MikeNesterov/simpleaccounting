package com.nesterov.simpleaccounting.repository;

import com.nesterov.simpleaccounting.entity.WalletEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletRepository extends JpaRepository<WalletEntity, Long>{
}
