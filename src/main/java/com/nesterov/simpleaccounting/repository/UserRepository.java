package com.nesterov.simpleaccounting.repository;

import com.nesterov.simpleaccounting.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long>{

    UserEntity findByLogin(String login);

}
