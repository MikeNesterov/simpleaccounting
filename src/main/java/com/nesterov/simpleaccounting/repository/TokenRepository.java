package com.nesterov.simpleaccounting.repository;

import com.nesterov.simpleaccounting.entity.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<TokenEntity, Long> {
    TokenEntity findByToken(String token);
}
