package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.entity.TokenEntity;
import com.nesterov.simpleaccounting.entity.UserEntity;

import java.util.List;

public interface UserService {

    List<UserEntity> getAll();
    UserEntity getById(long id);
    UserEntity save(UserEntity user);
    void delete(long id);
    UserEntity getByLogin(String login);
    void newPassword(UserDto userDto);
    void confirmRegistration(TokenEntity token);
    boolean checkUserInDB(String login);
    UserEntity createUser(UserDto userDto);

}
