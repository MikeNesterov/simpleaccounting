package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.entity.UserEntity;
import com.nesterov.simpleaccounting.entity.WalletEntity;

import javax.validation.Valid;

public interface WalletService {
    void delete(long id);
    WalletEntity getById(long id);
    WalletEntity save(WalletEntity walletEntity);
    void updateWallet(long wallet_id, WalletEntity walletEntity);
    void createWallet(long user_id, WalletEntity walletEntity);
}
