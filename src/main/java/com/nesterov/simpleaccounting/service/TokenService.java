package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.entity.TokenEntity;
import com.nesterov.simpleaccounting.entity.UserEntity;

public interface TokenService {

    TokenEntity save(TokenEntity token);
    TokenEntity getToken(String token);
    void delete(TokenEntity token);
    TokenEntity createToken(UserEntity user);
    boolean validateToken(TokenEntity token);
}
