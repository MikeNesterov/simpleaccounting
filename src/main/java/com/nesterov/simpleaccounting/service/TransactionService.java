package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.entity.TransactionEntity;

import javax.validation.Valid;

public interface TransactionService {
    void delete(long id);
    TransactionEntity getById(long id);
    TransactionEntity save(TransactionEntity transactionEntity);
    void updateTransaction(long transaction_id, TransactionEntity transactionEntity);
    void addTransaction(long wallet_id, TransactionEntity transactionEntity);
}
