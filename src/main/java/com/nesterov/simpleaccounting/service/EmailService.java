package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.entity.UserEntity;

public interface EmailService {
    boolean sendEmail (UserEntity user, String text);
    boolean sendRegistrationEmail (UserDto user);
    boolean sendNewPasswordEmail (UserDto user);
}
