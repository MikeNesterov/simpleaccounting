package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.entity.TransactionEntity;
import com.nesterov.simpleaccounting.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private WalletService walletService;

    @Override
    public void delete(long id) {
        transactionRepository.deleteById(id);
    }

    @Override
    public TransactionEntity getById(long id) {
        return transactionRepository.findById(id).get();
    }

    @Override
    public TransactionEntity save(TransactionEntity transactionEntity) {
        return transactionRepository.save(transactionEntity);
    }

    @Override
    public void updateTransaction(long transaction_id, TransactionEntity transactionEntity) {
        TransactionEntity transaction = getById(transaction_id);
        if(transaction == null)
            throw new NotFoundException("transaction not found");

        transaction.setComment(transactionEntity.getComment());
        transaction.setDate(transaction.getDate());
        transaction.setValue(transaction.getValue());

        save(transaction);
    }

    @Override
    public void addTransaction(long wallet_id, TransactionEntity transactionEntity) {
        transactionEntity.setWallet(walletService.getById(wallet_id));
        save(transactionEntity);
    }
}
