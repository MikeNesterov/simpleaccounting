package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.entity.TokenEntity;
import com.nesterov.simpleaccounting.entity.UserEntity;
import com.nesterov.simpleaccounting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UserEntity> getAll() {
        return repository.findAll();
    }

    public UserEntity getById(long id) {
        return repository.findById(id).get();
    }

    public UserEntity save(UserEntity user) {
        return repository.saveAndFlush(user);
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

    public UserEntity getByLogin(String login) {
        return repository.findByLogin(login);
    }

    @Override
    public void newPassword(UserDto userDto) {
        UserEntity userEntity = getByLogin(userDto.getEmail());
        userEntity.setPassword(passwordEncoder.encode(userDto.getPassword()));
        save(userEntity);
    }

    @Override
    public void confirmRegistration(TokenEntity token) {
        UserEntity user = token.getUser();
        user.setEnabled(true);
        save(user);
        tokenService.delete(token);
    }

    @Override
    public boolean checkUserInDB(String login) {
        if(userService.getByLogin(login)!=null) { return true; }
        return false;
    }

    @Override
    public UserEntity createUser(UserDto userDto) {
        UserEntity userEntity = new UserEntity();
        userEntity.setLogin(userDto.getEmail());
        userEntity.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userEntity.setFirstName(userDto.getFirstName());
        userEntity.setLastName(userDto.getLastName());

        return userEntity;
    }
}
