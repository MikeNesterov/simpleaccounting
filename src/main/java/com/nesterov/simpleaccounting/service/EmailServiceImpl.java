package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.dto.UserDto;
import com.nesterov.simpleaccounting.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Properties;
import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
@Transactional
@PropertySource("classpath:email.properties")
public class EmailServiceImpl implements EmailService {

    @Resource
    private Environment env;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @Override
    public boolean sendEmail(UserEntity user, String text) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(env.getRequiredProperty("email.mainEmail"), env.getRequiredProperty("email.mainPass"));
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(env.getRequiredProperty("email.mainEmail")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(user.getLogin()));
            message.setSubject(env.getRequiredProperty("email.subject"));
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sendRegistrationEmail(UserDto user) {
        UserEntity userEntity = userService.createUser(user);
        userService.save(userEntity);
        String text = String.format(env.getRequiredProperty("email.sendRegistrationEmail"),
                userEntity.getFirstName(),tokenService.createToken(userEntity).getToken());
        return sendEmail(userEntity, text);
    }

    @Override
    public boolean sendNewPasswordEmail(UserDto user) {
        UserEntity userEntity = userService.getByLogin(user.getEmail());
        String text = String.format(env.getRequiredProperty("email.sendNewPasswordEmail"),
                userEntity.getFirstName(),tokenService.createToken(userEntity).getToken());
        return sendEmail(userEntity, text);
    }
}
