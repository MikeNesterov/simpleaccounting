package com.nesterov.simpleaccounting.service;

import com.nesterov.simpleaccounting.entity.UserEntity;
import com.nesterov.simpleaccounting.entity.WalletEntity;
import com.nesterov.simpleaccounting.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private UserService userService;

    @Override
    public void delete(long id) {
        walletRepository.deleteById(id);
    }

    @Override
    public WalletEntity getById(long id) {
        return walletRepository.findById(id).get();
    }

    @Override
    public WalletEntity save(WalletEntity walletEntity) {
        return walletRepository.save(walletEntity);
    }

    @Override
    public void updateWallet(long wallet_id, WalletEntity walletEntity) {
        WalletEntity wallet = getById(wallet_id);
        if(wallet == null)
            throw new NotFoundException("wallet not found");

        wallet.setName(walletEntity.getName());
        wallet.setCurrency(walletEntity.getCurrency());

        save(wallet);
    }

    @Override
    public void createWallet(long user_id, WalletEntity walletEntity) {
        walletEntity.setUser(userService.getById(user_id));
        save(walletEntity);
    }
}
