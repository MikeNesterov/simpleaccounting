# README #

Java web application allows you to keep simple accounts of income and expenses. 
Implemented registration and authorization of users. Information about wallets and transactions stored in the database. 
I used the following technologies:

- Spring (MVC, Data, Security)
- JSP/Hibernate
- MySql
- Maven
- Tomcat
